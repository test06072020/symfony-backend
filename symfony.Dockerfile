FROM php:7.4-cli

RUN apt-get update && apt-get install -y \
      wget \
      git \
      fish

RUN apt-get update && apt-get install -y libzip-dev && docker-php-ext-install pdo zip

# Support de MySQL (pour la migration)
RUN docker-php-ext-install mysqli pdo_mysql

# Install Redis extension
RUN pecl install -o -f redis && rm -rf /tmp/pear && docker-php-ext-enable redis

# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

# Symfony tool
RUN wget https://get.symfony.com/cli/installer -O - | bash && \
  mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /usr/src/myapp
COPY . .

# Network issue - Cant connect to Composer
#RUN composer global require hirak/prestissimo
#RUN composer require amphp/http-client
RUN symfony server:ca:install

CMD symfony server:start
